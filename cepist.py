# -*- coding: utf-8 -*-
import sys
from PySide.QtGui import *
from PySide.QtCore import *
from mainwindow import Ui_MainWindow
from iperf_interface import IperfInterface

VERSION = '0.08.23'
MINOR_V = 'd'
settings = QSettings('settings.ini', QSettings.IniFormat)
# 5.172.249.2 or 5.172.249.10
SERVER = settings.value('server', 'lte:lte@5.172.249.2:22')


class Main(QMainWindow):
    def __init__(self, parent=None):
        super(Main, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.setWindowTitle(self.windowTitle() + ' ' + VERSION + MINOR_V)
        self.setWindowIcon(QIcon('logo.ico'))

        self.configure()
        self.load_defaults()

        self.__iperService = None

    @property
    def duration(self):
        return self.ui.wDuration.value()

    @property
    def bandwidth(self):
        return self.ui.wBandwidth.value()

    @property
    def target_ip(self):
        return self.ui.wTarget.text()

    @property
    def parallel_connections(self):
        return self.ui.wConnections.value()

    @property
    def mode(self):
        return self.ui.wMode.itemData(self.ui.wMode.currentIndex())

    @property
    def udp_test(self):
        return self.ui.wType.itemData(self.ui.wType.currentIndex())

    @property
    def iperf_locally(self):
        return self.ui.wIperfLoc.itemData(self.ui.wIperfLoc.currentIndex())

    @property
    def reverse(self):
        return self.ui.wReverse.isChecked()

    def configure(self):
        self.ui.wTarget.setInputMask('000.000.000.000')
        self.ui.wDuration.setSuffix(' s')
        self.ui.wBandwidth.setSuffix(' Mbps')

        self.ui.wLaunchButton.toggled.connect(self.on_test_start_stop)
        self.ui.wType.currentIndexChanged.connect(self.on_type_change)
        self.ui.wReverse.stateChanged.connect(self.on_reversemode)

        mode = self.ui.wMode
        mode.clear()
        mode.addItem(self.tr(
            'Default'), userData='')
        mode.addItem(self.tr(
            'Bi-directional (simultaneusly)'), userData='-d')
        mode.addItem(self.tr(
            'Bi-directional (one after another)'), userData='-r')

        type = self.ui.wType
        type.clear()
        type.addItem('UDP', userData=True)
        type.addItem('TCP', userData=False)

        iperf_loc = self.ui.wIperfLoc
        iperf_loc.clear()
        iperf_loc.addItem(self.tr('Remote mode'), userData=False)
        iperf_loc.addItem(self.tr('Local mode'), userData=True)

        # manu actions
        self.ui.actionReset_to_default_settings.triggered.connect(
            self.on_settings_reset)

    def on_settings_reset(self):
        import os
        global settings
        settings_file = 'settings.ini'
        if os.path.exists(settings_file):
            os.remove(settings_file)
        settings = QSettings('settings.ini', QSettings.IniFormat)
        self.load_defaults()

    def on_reversemode(self, value):
        state = value == Qt.CheckState.Unchecked
        self.ui.wMode.setCurrentIndex(0)
        self.ui.wMode.setEnabled(state)
        self.ui.wReverse.setText(
            self.tr('DUT <- server') if state else self.tr('DUT -> server'))

    def on_type_change(self):
        self.ui.wBandwidth.setEnabled(
            self.udp_test and not self.ui.wReverse.isChecked())
        self._updateHint()

    def _updateHint(self):
        mode = '-s'
        udp = ' -u' if self.udp_test else ''
        self.ui.wHint.setText('%s%s -i 5' % (mode, udp))

    def on_test_start_stop(self, state):
        button = self.ui.wLaunchButton
        if state is True:
            self.__iperService = IperfInterface(
                SERVER, self, self.iperf_locally)
            self.__iperService.start()
            button.setText(self.tr('STOP'))
            self.ui.wOutput.clear()
        else:
            if self.__iperService and self.__iperService.is_alive():
                self.__iperService.kill()
            button.setText(self.tr('START'))

    def load_defaults(self):
        bw = settings.value('bandwidth', 40)
        dur = settings.value('duration', 60)
        target_ip = settings.value('target_ip', '172.16.223.2')
        parallel_connections = settings.value('parallel_connections', 1)
        iperf_servermode = settings.value('servermode', False)

        # combo boxes
        mode_option = settings.value('mode_option', 0)
        type_option = settings.value('type_option', 0)
        iperfloc_option = settings.value('iperfloc_option', 0)

        self.ui.wTarget.setText(target_ip)
        self.ui.wBandwidth.setValue(int(bw))
        self.ui.wDuration.setValue(int(dur))
        self.ui.wConnections.setValue(int(parallel_connections))
        self.ui.wReverse.setChecked(iperf_servermode == 'true')
        try:
            self.ui.wMode.setCurrentIndex(int(mode_option))
            self.ui.wType.setCurrentIndex(int(type_option))
            self.ui.wIperfLoc.setCurrentIndex(int(iperfloc_option))
        except:
            pass

    def save_defaults(self):
        settings.setValue('server', SERVER)
        settings.setValue('target_ip', self.ui.wTarget.text())
        settings.setValue('bandwidth', self.ui.wBandwidth.value())
        settings.setValue('duration', self.ui.wDuration.value())
        settings.setValue('parallel_connections', self.ui.wConnections.value())
        settings.setValue('servermode', self.ui.wReverse.isChecked())

        settings.setValue('mode_option', self.ui.wMode.currentIndex())
        settings.setValue('type_option', self.ui.wType.currentIndex())
        settings.setValue('iperfloc_option', self.ui.wIperfLoc.currentIndex())

    def closeEvent(self, event):
        self.save_defaults()

    def notify(self, message):
        bar = self.statusBar()
        bar.showMessage(message)

    def log(self, message):
        w = self.ui.wOutput
        w.append(message.replace('\r\n', '\r'))


if __name__ == "__main__":
    import qdarkstyle
    from os.path import join
    translator = QTranslator()
    lang = QLocale.system().name()
    lang = settings.value('lang', lang)
    settings.setValue('lang', lang)
    translator.load(join('i18n', lang))
    app = QApplication(sys.argv)
    app.installTranslator(translator)
    app.setStyleSheet(qdarkstyle.load_stylesheet())
    mySW = Main()
    mySW.show()

    sys.exit(app.exec_())
