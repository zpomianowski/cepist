<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1">
<context>
    <name>IperfInterface</name>
    <message>
        <location filename="iperf_interface.py" line="30"/>
        <source>Local test aborted!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="41"/>
        <source>Test aborted!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="104"/>
        <source>Please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="112"/>
        <source>Connected!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="119"/>
        <source>Running...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="125"/>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="129"/>
        <source>Server authentication error!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="137"/>
        <source>Unidentified connection error!  Does this server even exist?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="cepist.py" line="73"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cepist.py" line="75"/>
        <source>Bi-directional (simultaneusly)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cepist.py" line="77"/>
        <source>Bi-directional (one after another)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cepist.py" line="86"/>
        <source>Remote mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cepist.py" line="87"/>
        <source>Local mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cepist.py" line="107"/>
        <source>DUT &lt;- server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cepist.py" line="107"/>
        <source>DUT -&gt; server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cepist.py" line="125"/>
        <source>STOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cepist.py" line="130"/>
        <source>START</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.py" line="143"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Iperf params for DUT&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="144"/>
        <source>DUT target IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="145"/>
        <source> Bandwidth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="146"/>
        <source>Test duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="147"/>
        <source>Parallel connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="148"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="149"/>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="150"/>
        <source>START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="151"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="152"/>
        <source>Reset to default settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
