<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl_PL">
<context>
    <name>IperfInterface</name>
    <message>
        <location filename="iperf_interface.py" line="30"/>
        <source>Local test aborted!</source>
        <translation>No i kurwa przerwałeś lokalny test zjebie!</translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="41"/>
        <source>Test aborted!</source>
        <translation>Test przerwany - zjebałeś!</translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="104"/>
        <source>Please wait...</source>
        <translation>Czekaj kurwa...</translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="112"/>
        <source>Connected!</source>
        <translation>Jest kurwa! Połączyłeś się geniuszu!</translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="119"/>
        <source>Running...</source>
        <translation>Trwa napierdalanie...</translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="125"/>
        <source>Idle</source>
        <translation>Chuj, pierdole. Nic nie robię. Odpoczywam!</translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="129"/>
        <source>Server authentication error!!!</source>
        <translation>I co żeś kurwa zrobił temu serweru?! Błąd autoryzacji cieciu!</translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="133"/>
        <source>SSH connection error!!!</source>
        <translation>W jakiej jesteś sieci ulungu pierdolony!? Czemu SSH nie działa?</translation>
    </message>
    <message>
        <location filename="iperf_interface.py" line="137"/>
        <source>Unidentified connection error!  Does this server even exist?</source>
        <translation>No i chuj! Błąd połączenia. W dobrej sieci jesteś w ogóle kutasiarzu?!</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="cepist.py" line="73"/>
        <source>Default</source>
        <translation>Domyślne jełopie</translation>
    </message>
    <message>
        <location filename="cepist.py" line="75"/>
        <source>Bi-directional (simultaneusly)</source>
        <translation>DL/UL naraz chuju</translation>
    </message>
    <message>
        <location filename="cepist.py" line="77"/>
        <source>Bi-directional (one after another)</source>
        <translation>DL i UL po sobie cepie</translation>
    </message>
    <message>
        <location filename="cepist.py" line="86"/>
        <source>Remote mode</source>
        <translation>Zdalnie kurwa</translation>
    </message>
    <message>
        <location filename="cepist.py" line="87"/>
        <source>Local mode</source>
        <translation>Lokalnie no ja pierdole</translation>
    </message>
    <message>
        <location filename="cepist.py" line="107"/>
        <source>DUT &lt;- server</source>
        <translation>Chujostwo &lt;- serwer</translation>
    </message>
    <message>
        <location filename="cepist.py" line="107"/>
        <source>DUT -&gt; server</source>
        <translation>Chujostwo -&gt; serwer</translation>
    </message>
    <message>
        <location filename="cepist.py" line="125"/>
        <source>STOP</source>
        <translation>STOP KURWA! PRRRRR....</translation>
    </message>
    <message>
        <location filename="cepist.py" line="130"/>
        <source>START</source>
        <translation>NAKURWIAJ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.py" line="143"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Iperf params for DUT&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;I tak nie zrozumiesz&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="144"/>
        <source>DUT target IP</source>
        <translation>No zgadnij co to kurwa</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="145"/>
        <source> Bandwidth</source>
        <translation>Mega-kurwa-bity</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="146"/>
        <source>Test duration</source>
        <translation>Czas nakurwiania</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="147"/>
        <source>Parallel connections</source>
        <translation>Połączeń nakurwiających</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="148"/>
        <source>Advanced</source>
        <translation>Ustawienia nie dla CIebie</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="149"/>
        <source>DUT &lt;- server</source>
        <translation>Chujostwo &lt;- serwer</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="151"/>
        <source>Settings</source>
        <translation>No ustawienia kurwa mać</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="152"/>
        <source>Reset to default settings</source>
        <translation>Resetuj rychło kurwa jego mać</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="150"/>
        <source>START</source>
        <translation>NAKURWIAJ</translation>
    </message>
</context>
</TS>
