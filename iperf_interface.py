# -*- coding: utf-8 -*-
from threading import Thread
from urlparse import urlparse
from paramiko import SSHClient, AutoAddPolicy
from paramiko.ssh_exception import AuthenticationException, SSHException
from PySide.QtCore import QObject
from utils import INVOKER


class IperfInterface(QObject, Thread):
    def __init__(self, conn_string, parent, run_locally):
        Thread.__init__(self)
        QObject.__init__(self)
        self.__parent = parent

        self.__parsed = urlparse('http://%s' % conn_string)
        self._client = None
        self._stopflag = False

        self.__locally = run_locally
        self.__localproc = None

    def kill(self):
        if self.__locally:
            if self.__localproc:
                self.__localproc.kill()
                self.__localproc = None
                INVOKER.invoke(
                    self.__parent.notify,
                    self.tr('Local test aborted!'))
            return

        self._stopflag = True
        INVOKER.invoke(
            self.__parent.ui.wLaunchButton.setEnabled,
            False)
        if self._client:
            self._client.close()
            INVOKER.invoke(
                self.__parent.notify,
                self.tr('Test aborted!'))
            INVOKER.invoke(
                self.__parent.ui.wLaunchButton.setEnabled,
                True)

    def is_connected(self):
        if self.__locally:
            return False
        transport = self._client.get_transport() if self._client else False
        return transport and transport.is_active()

    def connect(self):
        if self.__locally:
            return
        vars = self.__parsed
        client = SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(AutoAddPolicy())
        client.connect(
            hostname=vars.hostname,
            port=vars.port,
            username=vars.username,
            password=vars.password,
            timeout=5,
            auth_timeout=10)
        self._client = client

    def _build_cmd(self):
        parent = self.__parent
        cmd = 'iperf -c %(target)s -i 5 -t %(duration)s %(mode)s'
        cmd = cmd % dict(
            target=parent.target_ip,
            duration=parent.duration,
            mode=parent.mode)
        pthreads = parent.parallel_connections
        if pthreads > 1:
            cmd = '%s -P %d' % (cmd.strip(), pthreads)
        if parent.udp_test:
            cmd = '%s -u -b %dm' % (cmd.strip(), parent.bandwidth)
        if parent.reverse:
            cmd += ' -R'
        return cmd

    def _exe(self, cmd):
        if self.__locally:
            from subprocess import Popen, PIPE
            import platform
            if platform.system() == 'Linux':
                iperf_path = 'iperf'
            else:
                iperf_path = 'tools\\iperf.exe'
            cmd = cmd.replace('iperf', iperf_path)
            p = Popen(cmd.split(' '), stdout=PIPE, stderr=PIPE)
            self.__localproc = p
            return (None, p.stdout, p.stderr)
        else:
            return self._client.exec_command(cmd, get_pty=True)

    def run(self):
        parent = self.__parent
        if self.is_connected() is not True:
            INVOKER.invoke(
                parent.notify,
                self.tr('Please wait...'))
            try:
                self.connect()
                if self._stopflag:
                    self.kill()
                    return

                INVOKER.invoke(parent.notify,
                               self.tr('Connected!'))

                cmd = self._build_cmd()
                INVOKER.invoke(parent.log, '>>> ' + cmd)
                # To get remote PID 'echo $$; exec CMD' and stdout.readline()
                stdin, stdout, stderr = self._exe(self._build_cmd())
                INVOKER.invoke(parent.notify,
                               self.tr('Running...'))
                for line in iter(lambda: stdout.readline(2048), ""):
                    INVOKER.invoke(parent.log, line.strip())

                INVOKER.invoke(
                    parent.notify,
                    self.tr('Idle'))
            except AuthenticationException:
                INVOKER.invoke(
                    parent.notify,
                    self.tr('Server authentication error!!!'))
            except SSHException:
                INVOKER.invoke(
                    parent.notify,
                    self.tr('SSH connection error!!!'))
            except:
                INVOKER.invoke(
                    parent.notify,
                    self.tr('Unidentified connection error!  Does this server even exist?'))
            finally:
                INVOKER.invoke(
                    self.__parent.ui.wLaunchButton.setChecked,
                    False)
