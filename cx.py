#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from cx_Freeze import setup, Executable
from cepist import VERSION

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {
    "packages":
        ["os", "cffi", "cryptography"],
    "excludes": ["tkinter"],
    "optimize": 2,
    "append_script_to_exe": True,
    "include_in_shared_zip": True,
    "include_files": [
        "logo.ico",
        ("i18n/pl_PL.qm", "i18n/pl_PL.qm"),
        ("i18n/en_GB.qm", "i18n/en_GB.qm"),
        ("i18n/vulg.qm", "i18n/vulg.qm"),
        "tools/"],
    "icon": "logo.ico"}

shortcut_table = [
    ("DesktopShortcut",         # Shortcut
     "DesktopFolder",           # Directory_
     "CePIST",                  # Name
     "TARGETDIR",               # Component_
     "[TARGETDIR]\cepist.exe",  # Target
     None,                      # Arguments
     None,                      # Description
     None,                      # Hotkey
     None,                      # Icon
     None,                      # IconIndex
     None,                      # ShowCmd
     "TARGETDIR"                # WkDir
     )]

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
options = {
    "build_exe": build_exe_options,
    }

if sys.platform == "win32":
    base = "Win32GUI"
    options = {
        "build_exe": build_exe_options,
        "bdist_msi": {
            "initial_target_dir": "c:\cepist",
            "data": {"Shortcut": shortcut_table}
            }
        }

setup(name="CePIST",
      version=VERSION,
      description="CePIST - CP Iperf Speed Test Tool",
      options=options,
      executables=[Executable("cepist.py", base=base, icon="logo.ico")])
