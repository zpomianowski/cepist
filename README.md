# __CePIST__ - _CP Iperf Speed Test Tool_

Narzędzie ma na celu upraszczac testy throughputów UDP/TCP w wariantach DL i UL. Przed użyciem poniższej aplikacji, należy odpowiednio skonfigurować urządzenie, które jest testowane, tzw. __DUT__ - _Device Under Test_.

Przykład dla telefonu _Android_:

*  zainstaluj apkę [_Iperf_](https://play.google.com/store/apps/details?id=com.magicandroidapps.iperf&hl=pl) na telefonie
*  odpal _Iperf_ w odpowiednim trybie serwera
*  ogólny format: ```-s [-u] -i <co_ile_sekund_raport>```
    *  ```-s -i 5``` dla testów __TCP__
    *  ```-s -u -i 5``` dla testów __UDP__
*  odczytaj __IP__ urządzenia __DUT__
*  odpal __CePIST__ i uzupełnij pola wg powyższej konfiguracji

__Uwagi__:

* __CePIST__ uruchom w środowisku (sieci), która zapewnia dostęp do serwerów testujących
    * `5.172.249.2` i `5.172.249.10`
* w testowanym urządzeniu (telefonie) musi być odpowiednia karta _SIM_ oraz ustawiony
odpowiedni _APN_ (ustawienia, które także zapewnią komunikację z serwerami testującymi)

![screenshot.png](/screenshot.png)

Twórca: [Zbigniew Pomianowski <zpomianowski@cyfrowypolsat.pl>](mailto:zpomianowski@cyfrowypolsat.pl)

## TODOs
*  Translacje (póki co chyba nie mają wysokiego priorytetu)

## Dev
Zanim przejdziesz dalej, upewnij się, że środowisko Windows/Linux jest gotowe.
### Dev Windows
```
cd cepist
c:\Python2.7\Scripts\virtualenv venv
venv\Scripts\activate
pip install -r requirements.txt
```

###### Generowanie instalatora Windows

Do tego użyta jest paczka *cxFreeze*. Konfiguracja w pliku *cx.py*. Listę komend znajdziesz wpisują: ``` python cx.py --help-commmands ```

Generowanie instalatora Windowsowego odbywa się poprzez komendę:
```
python cx.py bdist_wininst
```

Instalator znajdziesz w nowym folderze *dist*.

### Dev Ubuntu
Na linuksie PySide musi być zbuildowany. Potrzebne są biblioteki devowe. Kompilacja PySide trwa... długo.
```
sudo apt install cmake libqt4-dev qt4-qmake
cd cepist
virtualenv venv
source venv\bin\activate
pip install -r requirements.txt
```
Jeśli jest problem z instalajcą _cx_Freeze_:

*  ```cd /tmp/pip-build*/cx_Freeze``` albo ściągnij paczkę _*.tar.gz_
*  Zastosuj [_hack_](https://stackoverflow.com/questions/25107697/compiling-cx-freeze-under-ubuntu)
*  python setup.py install

###### Generowanie zbundlowanej apki
```
python cx.py build
```
Szukaj w folderze _build_.



### Więcej
A więcej znajdziesz na [stronie intranetowej wiki sekcji SAUT](http://saut-test.polsatc/atms/default/wiki/cepist).
Wszelkie formy zgłaszania bęłw, sugestii oraz poprawek mile widziane :)
