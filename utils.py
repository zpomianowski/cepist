# -*- coding: utf-8 -*-
from Queue import Queue
from PySide.QtCore import Qt, QMetaObject, Slot, QObject


class Invoker(QObject):
    def __init__(self):
        super(Invoker, self).__init__()
        self.queue = Queue()

    def invoke(self, func, *args, **kwargs):
        f = lambda: func(*args, **kwargs)
        self.queue.put(f)
        QMetaObject.invokeMethod(self, "handler", Qt.QueuedConnection)

    @Slot()
    def handler(self):
        f = self.queue.get()
        f()


INVOKER = Invoker()
